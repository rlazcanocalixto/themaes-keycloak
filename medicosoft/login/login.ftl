<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo displayWide=(realm.password && social.providers??); section>

    <#if section = "header">
        ${msg("doLogIn")}
    <#elseif section = "form">
    <div class="row" style="height: 100%;margin: 0;padding-top: 100px;align-content: center"> 
    <!---Value added by Medicosft---->
   <div class="col s12" style="margin:0;padding:0;">
    <style>
    .nl{
        display:flex;align-content:center;justify-content:center;
     
    }
    .iml{
       width:30%;
    }

    @media (max-width:768px) {
 .nl{
        display:flex;align-content:center;justify-content:center;
           margin:0px; padding:0px;
    }
    .iml{
       width:90%;
       padding:0px;
       margin:0px;
    }
        }
    </style>

    <!---Value added by Medicosft logo---->
   <div class="nl"> 
   <#include "./logo_hipo.ftl" >
   </div>
    </div>


    <script>console.log("${url.resourcesPath}",'hell');</script>


    <div class="col card hoverable s10 push-s1  m10 push-m1 l6 push-l3 xl4 push-xl4" style="background-color: #45bb3a;padding:20px;">
    <div id="kc-form" style=""  <#if realm.password && social.providers??>class="${properties.kcContentWrapperClass!}"</#if>>
      <div id="kc-form-wrapper" <#if realm.password && social.providers??>class="${properties.kcFormSocialAccountContentClass!} ${properties.kcFormSocialAccountClass!}"</#if>>
        <#if realm.password>
            <form id="kc-form-login" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">
               <!--added value by Medicosoft --->
                <div class="input-field col s12" style="background: white;">
                 <div class="row" style="margin:0px;">
                 <div class="col s1" ><i class="material-icons prefix" >person_outline</i>    
                 </div>
                 <div class="col s11 ${properties.kcFormGroupClass!}" style="margin:0px;">                                                 
                    <#if usernameEditDisabled??>
                        <input style="margin:0px" tabindex="1" id="username" class="${properties.kcInputClass!}" name="username" value="${(login.username!'')}"  type="text" placeholder="<#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>" autofocus autocomplete="off" " disabled />
                    <#else>
                        <input  tabindex="1" id="username" class="${properties.kcInputClass!}" name="username" value="${(login.username!'')}"  type="text" placeholder="<#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>" autofocus autocomplete="off" />
                    </#if>  
                </div>
                 </div>                 
                 </div>


                  <div class="input-field col s12" style="background: white;">
                 <div class="row" style="margin:0px;">
                 <div class="col s1"><i class="material-icons prefix">vpn_key</i>    
                 </div>
                 <div class="col s11 ${properties.kcFormGroupClass!}">                                                                   
                    <input tabindex="2" id="password" class="${properties.kcInputClass!}" name="password" type="password" placeholder="${msg("password")}" />
                </div>
                 </div>                 
                 </div>
                   


               
<!--
                <div class="${properties.kcFormGroupClass!}">
                    <label for="password" class="${properties.kcLabelClass!}">${msg("password")}</label>
                    <input tabindex="2" id="password" class="${properties.kcInputClass!}" name="password" type="password" autocomplete="off" />
                </div>
-->
                <div class="${properties.kcFormGroupClass!} ${properties.kcFormSettingClass!}">
                    <div id="kc-form-options">
                        <#if realm.rememberMe && !usernameEditDisabled??>
                            <div class="checkbox">
                                <label>
                                    <#if login.rememberMe??>
                                        <input tabindex="3" id="rememberMe" name="rememberMe" type="checkbox" checked> ${msg("rememberMe")}
                                    <#else>
                                        <input tabindex="3" id="rememberMe" name="rememberMe" type="checkbox"> ${msg("rememberMe")}
                                    </#if>
                                </label>
                            </div>
                        </#if>
                        </div>
                        <div class="${properties.kcFormOptionsWrapperClass!}">
                            <#if realm.resetPasswordAllowed>
                                <span><a tabindex="5" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></span>
                            </#if>
                        </div>

                  </div>

                  <div id="kc-form-buttons" class="${properties.kcFormGroupClass!}">
                    <input tabindex="4" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonBlockClass!} ${properties.kcButtonLargeClass!} btn green accent-4" name="login" id="kc-login" type="submit" value="${msg("doLogIn")}"
                    style="width:100%" />
                  </div>
            </form>
        </#if>
        </div>
        <#if realm.password && social.providers??>
            <div id="kc-social-providers" class="${properties.kcFormSocialAccountContentClass!} ${properties.kcFormSocialAccountClass!}">
                <ul class="${properties.kcFormSocialAccountListClass!} <#if social.providers?size gt 4>${properties.kcFormSocialAccountDoubleListClass!}</#if>">
                    <#list social.providers as p>
                        <li class="${properties.kcFormSocialAccountListLinkClass!}"><a href="${p.loginUrl}" id="zocial-${p.alias}" class="zocial ${p.providerId}"> <span>${p.displayName}</span></a></li>
                    </#list>
                </ul>
            </div>
        </#if>
      </div>
      </div>
      </div>
    <#elseif section = "info" >
        <#if realm.password && realm.registrationAllowed && !usernameEditDisabled??>
            <div id="kc-registration">
                <span>${msg("noAccount")} <a tabindex="6" href="${url.registrationUrl}">${msg("doRegister")}</a></span>
            </div>
        </#if>
    </#if>

</@layout.registrationLayout>
