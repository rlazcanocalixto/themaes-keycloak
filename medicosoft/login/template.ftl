<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayWide=false>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">
    <!--Materialize Mobile added value by Medicosfot-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>        
    <!--Materialize Compiled and minified CSS added value by Medicosfot-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!--favicon added value by Medicosoft --->
   
    <!--iconos Materialize added value by Medicosoft --->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"  type="text/css" rel="stylesheet">
    <!--Styles added value by Medicosoft --->
    <!--https://gitlab.com/rlazcanocalixto/themaes-keycloak/raw/master/medicosoft/login/resources/favicon.ico-->
    <link rel="icon" href="https://gitlab.com/rlazcanocalixto/themaes-keycloak/raw/master/medicosoft/login/resources/favicon.ico" />

    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <!--<title>${msg("loginTitle",(realm.displayName!''))}</title>-->
    <!--added value by Medicosoft --->
    <title>Hipocrates</title>
    <!--<link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />-->
    <link rel="icon" type="image/x-icon" href="${url.resourcesPath}/login/resources/favicon.ico">
    <script>console.log("${url.resourcesPath}",'hell');</script>

    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
            
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript">
            </script>
        </#list>
    </#if>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>
   
</head>

<body class="${properties.kcBodyClass!}" style="height: 100%; margin: 0;" >
  <div class="${properties.kcLoginClass!}" style="height: 100%; margin: 0;">
  <!--
    <div id="kc-header" class="${properties.kcHeaderClass!}" style="background-color:gray;">
      <div id="kc-header-wrapper" class="${properties.kcHeaderWrapperClass!}">${kcSanitize(msg("loginTitleHtml",(realm.displayNameHtml!'')))?no_esc}</div>
    </div>
    -->
    <div class="${properties.kcFormCardClass!} <#if displayWide>${properties.kcFormCardAccountClass!}</#if> colorU" style="height:100%;margin:0;">
      <header class="${properties.kcFormHeaderClass!}">
       <#if realm.internationalizationEnabled  && locale.supported?size gt 1>
            <div id="kc-locale">
                <div id="kc-locale-wrapper" class="${properties.kcLocaleWrapperClass!}">
                    <div class="kc-dropdown" id="kc-locale-dropdown">
                        <a href="#" id="kc-current-locale-link">${locale.current}</a>
                        <ul>
                            <#list locale.supported as l>
                                <li class="kc-dropdown-item"><a href="${l.url}">${l.label}</a></li>
                            </#list>
                        </ul>
                    </div>
                </div>
            </div>
        </#if>
        <!--Removed value by Medicosoft --->
     <!--   <h1 id="kc-page-title"><#nested "header"></h1>-->
      </header>
      <div id="kc-content">
        <div id="kc-content-wrapper">

          <#if displayMessage && message?has_content>
              <div class="alert alert-${message.type}">
                  <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                  <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                  <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                  <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                  <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
              </div>
          </#if>

          <#nested "form">

          <#if displayInfo>
              <div id="kc-info" class="${properties.kcSignUpClass!}">
                  <div id="kc-info-wrapper" class="${properties.kcInfoAreaWrapperClass!}">
                      <#nested "info">
                  </div>
              </div>
          </#if>
        </div>
      </div>

    </div>
  </div>
  
    <!-- Compiled and minified JavaScript  added value by Medicosoft-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <!-- Compiled jQuery JavaScript  added value by Medicosoft-->
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
</body>
</html>
</#macro>
